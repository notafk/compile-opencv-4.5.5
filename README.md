**You must have installed on your machine ubuntu 20.0.04 :**
- python2.7 & python 3.9
- gcc-8
- wget
- zip
- Qt5 (Setup launcher for Qt [here](https://www.qt.io/download-open-source#section-2) )

# This are general dependencies that you'll need.
```
$ sudo apt install build-essential cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev \
python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libdc1394-22-dev python3-pip python3-numpy
```
# Mesa package for QT=ON compilation
```
$ sudo apt install mesa-common-dev libglu1-mesa-dev
```
More details on requirements of opencv compilation [here](https://docs.opencv.org/4.5.5/d7/d9f/tutorial_linux_install.html) + [here](https://developer.ridgerun.com/wiki/index.php?title=Compiling_OpenCV_from_Source)

```
$ mkdir Proj_OpenCV
$ cd Proj_OpenCV
$ python3.9 -m venv env_opencv
$ source env_opencv/bin/activate
$ pip install numpy==1.22.3

$ wget -O opencv-4.5.5.zip https://github.com/opencv/opencv/archive/4.5.5.zip
$ unzip opencv-4.5.5.zip
$ cd opencv-4.5.5
$ mkdir build 
$ cd build

$ cmake -D CMAKE_BUILD_TYPE=RELEASE \
-D CMAKE_INSTALL_PREFIX=/usr/local \
-D OPENCV_GENERATE_PKGCONFIG=ON \
-D BUILD_EXAMPLES=OFF \
-D INSTALL_PYTHON_EXAMPLES=OFF \
-D INSTALL_C_EXAMPLES=OFF \
-D WITH_CSTRIPES=ON \
-D WITH_OPENMP=ON \
-D WITH_IPP=ON \
-D WITH_TBB=ON \
-D WITH_NVCUVID=OFF \
-D WITH_CUDA=OFF \
-D BUILD_opencv_cudacodec=OFF \
-D ENABLE_FAST_MATH=1 \
-D CUDA_FAST_MATH=1 \
-D WITH_CUBLAS=OFF \
-D WITH_V4L=ON \
-D WITH_OPENCL=OFF \
-D WITH_GSTREAMER=OFF \
-D OPENCV_SKIP_PYTHON_LOADER=ON \
-D WITH_QT=ON \
-D QT_QMAKE_EXECUTABLE=/opt/Qt5/5.15.2/gcc_64/bin/qmake \
-D Qt5_DIR=/opt/Qt5/5.15.2/gcc_64/lib/cmake/Qt5 \
-D PYTHON_EXECUTABLE=$(which python2) \
-D BUILD_opencv_python2=OFF \
-D PYTHON3_EXECUTABLE=$(which python3.9) \
-D PYTHON3_INCLUDE_DIR=$(python3.9 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
-D PYTHON3_PACKAGES_PATH=$(python3.9 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
 ..
```

```
make j-12
sudo make install
```


This will give an **.cv2.cpython-39-x86_64-linux-gnu.so** file in your env_opencv/lib/python3.9/site-packages <br/>
You can copy and paste on your other venv projects in the site-packages folder.<br/>
If you want to see the resume of flags activated, in the build folder :<br/>

`$ cmake -L`

